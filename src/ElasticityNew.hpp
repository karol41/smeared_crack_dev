/* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __ELASTICITYNEW_HPP__
#define __ELASTICITYNEW_HPP__
struct LoadScale : public MethodForForceScaling {

  static double lambda;

  MoFEMErrorCode scaleNf(const FEMethod *fe, VectorDouble &nf) {
    MoFEMFunctionBegin;
    nf *= lambda;
    MoFEMFunctionReturn(0);
  }
};

double LoadScale::lambda = 1;

// declaration
struct CommonData {
  vector<VectorDouble> dataAtGaussPts;
  vector<MatrixDouble> gradAtGaussPts;
  MatrixDouble D;
  MatrixDouble Consistent_Matrix;
  MatrixDouble Voigt_Unit_Matrix;
  MatrixDouble Unit_Matrix;
  MatrixDouble Flow_Tensor;
  VectorDouble Dev_Total_Strain;
  VectorDouble sTrain;
  vector<VectorDouble> sTress;
  VectorDouble Dev_trial_stress;
  VectorDouble Plastic_Strain;
  VectorDouble Dev_Plastic_Strain;
  vector<VectorDouble> Plastic_Strain_Temp;
  VectorDouble Flow_Vector;
  VectorDouble Voigt_Unit;
  int cOunter = 0;
  double tHeta;
  double tHeta_bar;
  double kAppa;
  double yIeld;
  double yOung;
  double pOisson;
  double lAme;
  double coefficient;
  double yIeld_Stress;
  double yIeld_Condition;
  double pLastic_Multiplier = 0;
  double tOlerance;

  MoFEMErrorCode getPlasticStrain(const EntityHandle fe_ent,
                                  const int nb_gauss_pts) {
    MoFEMFunctionBegin;
    double *tag_data;
    int tag_size;
    CHKERR mField.get_moab().tag_get_by_ptr(
        thPlasticStrain, &fe_ent, 1, (const void **)&tag_data, &tag_size);

    if (tag_size == 1) {
      Plastic_Strain.resize(nb_gauss_pts * 6);
      Plastic_Strain.clear();
      void const *tag_data[] = {&Plastic_Strain[0]};
      const int tag_size = Plastic_Strain.size();
      CHKERR mField.get_moab().tag_set_by_ptr(thPlasticStrain, &fe_ent, 1,
                                              tag_data, &tag_size);
    } else if (tag_size != nb_gauss_pts * 6) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Wrong size of the tag, data inconsistency");
    } else {
      VectorAdaptor tag_vec = VectorAdaptor(
          tag_size, ublas::shallow_array_adaptor<double>(tag_size, tag_data));
      Plastic_Strain = tag_vec;
    }
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode setPlasticStrain(
    const EntityHandle fe_ent, const int nb_gauss_pts
  ) {
    MoFEMFunctionBegin;
    void const *tag_data[] = {&Plastic_Strain[0]};
    const int tag_size = Plastic_Strain.size();
    CHKERR mField.get_moab().tag_set_by_ptr(thPlasticStrain, &fe_ent, 1,
                                              tag_data, &tag_size);
    MoFEMFunctionReturn(0);
  }

  CommonData(MoFEM::Interface &m_field) : mField(m_field) {

    // Setting coefficient

    yIeld = 0.;
    yIeld_Stress = 4e5;
    pOisson = 0.;
    yOung = 5000000;
    tOlerance = 1e-3;
    pLastic_Multiplier = 0.;
    coefficient = yOung / ((1 + pOisson) * (1 - 2 * pOisson));
    lAme = yOung / (2 * (1 + pOisson));
    kAppa = coefficient * pOisson + (2. / 3.) * lAme;

    // Build the D Matrix

    D.resize(6, 6, false);
    D.clear();

    D.insert_element(0, 0, 1 - pOisson);
    D.insert_element(1, 1, 1 - pOisson);
    D.insert_element(2, 2, 1 - pOisson);
    D.insert_element(3, 3, 0.5 * (1 - 2 * pOisson));
    D.insert_element(4, 4, 0.5 * (1 - 2 * pOisson));
    D.insert_element(5, 5, 0.5 * (1 - 2 * pOisson));

    D.insert_element(0, 1, pOisson);
    D.insert_element(0, 2, pOisson);
    D.insert_element(1, 0, pOisson);

    D.insert_element(1, 2, pOisson);
    D.insert_element(2, 0, pOisson);
    D.insert_element(2, 1, pOisson);

    D *= coefficient;

    // Set Initial Consistent Matrix

    Consistent_Matrix.resize(6, 6, false);
    Consistent_Matrix.clear();
    Consistent_Matrix = D;

    // Set Voigt_Unit_Matrix

    Voigt_Unit_Matrix.resize(6, 6, false);
    Voigt_Unit_Matrix.clear();

    Voigt_Unit_Matrix.insert_element(0, 0, 1);
    Voigt_Unit_Matrix.insert_element(1, 1, 1);
    Voigt_Unit_Matrix.insert_element(2, 2, 1);

    Voigt_Unit_Matrix.insert_element(0, 1, 1);
    Voigt_Unit_Matrix.insert_element(0, 2, 1);
    Voigt_Unit_Matrix.insert_element(1, 0, 1);

    Voigt_Unit_Matrix.insert_element(1, 2, 1);
    Voigt_Unit_Matrix.insert_element(2, 0, 1);
    Voigt_Unit_Matrix.insert_element(2, 1, 1);

    // Set Unit_Matrix

    Unit_Matrix.resize(6, 6, false);
    Unit_Matrix.clear();

    Unit_Matrix.insert_element(0, 0, 1);
    Unit_Matrix.insert_element(1, 1, 1);
    Unit_Matrix.insert_element(2, 2, 1);
    Unit_Matrix.insert_element(3, 3, 1);
    Unit_Matrix.insert_element(4, 4, 1);
    Unit_Matrix.insert_element(5, 5, 1);

    // Set Flow_Tensor matrix

    Flow_Tensor.resize(6, 6, false);
    Flow_Tensor.clear();

    ierr = createTags(m_field.get_moab());
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
  }

private:
  MoFEM::Interface &mField;
  Tag thPlasticStrain;

  MoFEMErrorCode createTags(moab::Interface &moab) {
    MoFEMFunctionBegin;
    std::vector<double> def_val(6, 0);
    CHKERR mField.get_moab().tag_get_handle(
        "PLASTIC_STRAIN", 1, MB_TYPE_DOUBLE, thPlasticStrain,
        MB_TAG_CREAT | MB_TAG_VARLEN | MB_TAG_SPARSE, &def_val[0]);
    MoFEMFunctionReturn(0);
  }
};

PetscErrorCode makeB(const MatrixAdaptor &diffN, MatrixDouble &B);

struct BlockOptionData {
  int oRder;
  double yOung;
  double pOisson;
  double initTemp;
  BlockOptionData() : oRder(-1), yOung(-1), pOisson(-2), initTemp(0) {}
};

struct OpAssembleK
    : public VolumeElementForcesAndSourcesCore::UserDataOperator {

  MatrixDouble rowB;
  MatrixDouble colB;
  MatrixDouble CB;
  MatrixDouble K, transK;

  CommonData &commonData;

  OpAssembleK(CommonData &common_data, bool symm = true)
      : VolumeElementForcesAndSourcesCore::UserDataOperator("U", "U", OPROWCOL,
                                                            symm),
        commonData(common_data) {}

  /**
   * \brief Do calculations for give operator
   * @param  row_side row side number (local number) of entity on element
   * @param  col_side column side number (local number) of entity on element
   * @param  row_type type of row entity MBVERTEX, MBEDGE, MBTRI or MBTET
   * @param  col_type type of column entity MBVERTEX, MBEDGE, MBTRI or MBTET
   * @param  row_data data for row
   * @param  col_data data for column
   * @return          error code
   */
  PetscErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type,
                        DataForcesAndSourcesCore::EntData &row_data,
                        DataForcesAndSourcesCore::EntData &col_data) {
    MoFEMFunctionBegin;
    // get number of dofs on row
    nbRows = row_data.getIndices().size();
    // if no dofs on row, exit that work, nothing to do here
    if (!nbRows)
      MoFEMFunctionReturnHot(0);
    // get number of dofs on column
    nbCols = col_data.getIndices().size();
    // if no dofs on Columbia, exit nothing to do here
    if (!nbCols)
      MoFEMFunctionReturnHot(0);
    // get number of integration points
    nbIntegrationPts = getGaussPts().size2();
    // chekk if entity block is on matrix diagonal
    if (row_side == col_side && row_type == col_type) { // ASK
      isDiag = true;                                    // yes, it is
    } else {
      isDiag = false;
    }
    // integrate local matrix for entity block
    CHKERR iNtegrate(row_data, col_data);
    // asseble local matrix
    CHKERR aSsemble(row_data, col_data);
    MoFEMFunctionReturn(0);
  }

protected:
  ///< error code

  int nbRows;           ///< number of dofs on rows
  int nbCols;           ///< number if dof on column
  int nbIntegrationPts; ///< number of integration points
  bool isDiag;          ///< true if this block is on diagonal

  /**
   * \brief Integrate grad-grad operator
   * @param  row_data row data (consist base functions on row entity)
   * @param  col_data column data (consist base functions on column entity)
   * @return          error code
   */
  virtual PetscErrorCode
  iNtegrate(DataForcesAndSourcesCore::EntData &row_data,
            DataForcesAndSourcesCore::EntData &col_data) {
    MoFEMFunctionBegin;

    int nb_dofs_row = row_data.getFieldData().size();
    if (nb_dofs_row == 0)
      PetscFunctionReturn(0);
    int nb_dofs_col = col_data.getFieldData().size();
    if (nb_dofs_col == 0)
      PetscFunctionReturn(0);

    K.resize(nb_dofs_row, nb_dofs_col, false);
    K.clear();
    CB.resize(6, nb_dofs_col, false);

    for (int gg = 0; gg != nbIntegrationPts; gg++) {
      // get element volume
      // get integration weight
      double val = getVolume() * getGaussPts()(3, gg);
      // ASK
      const MatrixAdaptor &diffN_row = row_data.getDiffN(gg, nb_dofs_row / 3);
      const MatrixAdaptor &diffN_col = col_data.getDiffN(gg, nb_dofs_col / 3);
      //     printf("\nGauss Point %d at row:\n", gg);
      CHKERR makeB(diffN_row, rowB);
      // printf("\nGauss Point %d at col:\n", gg);
      CHKERR makeB(diffN_col, colB);
       
      //  noalias(CB) = prod(commonData.Consistent_Matrix, colB);
       noalias(CB) = prod(commonData.D, colB);
       noalias(K) += val * prod(trans(rowB), CB);
      
     
    }
    MoFEMFunctionReturn(0);
  }

  /**
   * \brief Assemble local entity block matrix
   * @param  row_data row data (consist base functions on row entity)
   * @param  col_data column data (consist base functions on column entity)
   * @return          error code
   */
  virtual PetscErrorCode aSsemble(DataForcesAndSourcesCore::EntData &row_data,
                                  DataForcesAndSourcesCore::EntData &col_data) {
    PetscFunctionBegin;

    int nb_dofs_row = row_data.getFieldData().size();
    if (nb_dofs_row == 0)
      PetscFunctionReturn(0);
    int nb_dofs_col = col_data.getFieldData().size();
    if (nb_dofs_col == 0)
      PetscFunctionReturn(0);

    // get pointer to first global index on row
    const int *row_indices = &*row_data.getIndices().data().begin();
    // get pointer to first global index on column
    const int *col_indices = &*col_data.getIndices().data().begin();
    Mat B = getFEMethod()->ksp_B != PETSC_NULL ? getFEMethod()->ksp_B
                                               : getFEMethod()->snes_B;
    // assemble local matrix
    CHKERR MatSetValues(B, nbRows, row_indices, nbCols, col_indices,
                        &*K.data().begin(), ADD_VALUES);
    if (!isDiag && sYmm) {
      // if not diagonal term and since global matrix is symmetric assemble
      // transpose term.
      K = trans(K);
      CHKERR MatSetValues(B, nbCols, col_indices, nbRows, row_indices,
                          &*K.data().begin(), ADD_VALUES);
    }
    PetscFunctionReturn(0);
  }
};

PetscErrorCode makeB(const MatrixAdaptor &diffN, MatrixDouble &B) {

  PetscFunctionBegin;
  unsigned int nb_dofs = diffN.size1();
  B.resize(6, 3 * nb_dofs, false);
  B.clear();
  for (unsigned int dd = 0; dd < nb_dofs; dd++) {
    const double diff[] = {diffN(dd, 0), diffN(dd, 1), diffN(dd, 2)};
    const int dd3 = 3 * dd;
    for (int rr = 0; rr < 3; rr++) {
      B(rr, dd3 + rr) = diff[rr];
    }
    // gamma_xy
    B(3, dd3 + 0) = diff[1];
    B(3, dd3 + 1) = diff[0];
    // gamma_yz
    B(4, dd3 + 1) = diff[2];
    B(4, dd3 + 2) = diff[1];
    // gamma_xz
    B(5, dd3 + 0) = diff[2];
    B(5, dd3 + 2) = diff[0];
  }
  PetscFunctionReturn(0);
}

struct OpAssembleRhs
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  CommonData &commonData;
  VectorDouble nF;
  MatrixDouble B;

  OpAssembleRhs(CommonData &common_data)
      : VolumeElementForcesAndSourcesCore::UserDataOperator(
            "U", UserDataOperator::OPROW),
        commonData(common_data) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    PetscFunctionBegin;
    try {

      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0)
        PetscFunctionReturn(0);
      int nb_gauss_pts = data.getN().size1();
      nF.resize(nb_dofs, false);
      nF.clear();

      for (int gg = 0; gg < nb_gauss_pts; gg++) {

        double val = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }
        const MatrixAdaptor &diffN = data.getDiffN(gg, nb_dofs / 3);
        CHKERR makeB(diffN, B);

      

        // noalias(commonData.sTrain) = prod(B,values);
        // noalias(commonData.sTress) = prod(D,commonData.sTrain);
        noalias(nF) += val * prod(trans(B), commonData.sTress[gg]);
        // cerr << "nF " << nF << endl;
      }

      // cerr << side << " " << type << " " << data.getN().size1() << " "<<
      // data.getN().size2() << endl;

      int *indices_ptr;
      indices_ptr = &data.getIndices()[0];

      CHKERR VecSetValues(getFEMethod()->snes_f, nb_dofs, indices_ptr, &nF[0],
                          ADD_VALUES);

    } catch (const std::exception &ex) {
      ostringstream ss;
      ss << "throw in method: " << ex.what() << endl;
      SETERRQ(PETSC_COMM_SELF, 1, ss.str().c_str());
    }

    PetscFunctionReturn(0);
  }
};
// OpCalculateVectorFieldValues<3>
struct OpGetDataAtGaussPts
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {
  vector<VectorDouble> &valuesAtGaussPts;
  vector<MatrixDouble> &gradientAtGaussPts;
  const EntityType zeroAtType;

  OpGetDataAtGaussPts(const string field_name,
                      vector<VectorDouble> &values_at_gauss_pts,
                      vector<MatrixDouble> &gradient_at_gauss_pts)
      : VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        valuesAtGaussPts(values_at_gauss_pts),
        gradientAtGaussPts(gradient_at_gauss_pts), zeroAtType(MBVERTEX) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    PetscFunctionBegin;
    try {
      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0) {
        PetscFunctionReturn(0);
      }
      int nb_gauss_pts = data.getN().size1();
      int rank = data.getFieldDofs()[0]->getNbOfCoeffs();
      // initialize
      if (type == zeroAtType) {
        valuesAtGaussPts.resize(nb_gauss_pts);
        gradientAtGaussPts.resize(nb_gauss_pts);
        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          valuesAtGaussPts[gg].resize(rank, false);
          gradientAtGaussPts[gg].resize(rank, 3, false);
          valuesAtGaussPts[gg].clear();
          gradientAtGaussPts[gg].clear();
        }
      }
      VectorDouble &values = data.getFieldData();
      for (int gg = 0; gg < nb_gauss_pts; gg++) {
        VectorDouble &values_at_gauss_pts = valuesAtGaussPts[gg];
        MatrixDouble &gradient_at_gauss_pts = gradientAtGaussPts[gg];
        VectorAdaptor N = data.getN(gg, nb_dofs / rank);
        MatrixAdaptor diffN = data.getDiffN(gg, nb_dofs / rank);
        for (int dd = 0; dd < nb_dofs / rank; dd++) {
          for (int rr1 = 0; rr1 < rank; rr1++) {
            const double v = values[rank * dd + rr1];
            values_at_gauss_pts[rr1] += N[dd] * v;
            for (int rr2 = 0; rr2 < 3; rr2++) {
              gradient_at_gauss_pts(rr1, rr2) += diffN(dd, rr2) * v;
            }
          }
        }
      }
      

    } catch (const std::exception &ex) {
      ostringstream ss;
      ss << "throw in method: " << ex.what() << endl;
      SETERRQ(PETSC_COMM_SELF, 1, ss.str().c_str());
    }
    PetscFunctionReturn(0);
  }
};

struct OpCalculateStress
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface &mField;
  CommonData &commonData;

  OpCalculateStress(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      PetscFunctionReturn(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      PetscFunctionReturn(0);
    int nb_gauss_pts = data.getN().size1();
    EntityHandle fe_ent = getFEEntityHandle();
    CHKERR commonData.getPlasticStrain(fe_ent, nb_gauss_pts);

    //@todo:resize only first time
    commonData.sTress.resize(nb_gauss_pts);
    commonData.Plastic_Strain_Temp.resize(nb_gauss_pts);

    for (int gg = 0; gg < nb_gauss_pts; gg++) {

      

      /* auto calculate_total_strain = [=]() {
       MoFEMFunctionBegin;*/

      commonData.sTrain.resize(6, false);
      commonData.sTrain.clear();
      {
        for (int ii = 0; ii < 3; ii++) {
          commonData.sTrain[ii] = commonData.gradAtGaussPts[gg](ii, ii);
        }
        commonData.sTrain[3] = commonData.gradAtGaussPts[gg](0, 1) +
                               commonData.gradAtGaussPts[gg](1, 0);
        commonData.sTrain[4] = commonData.gradAtGaussPts[gg](1, 2) +
                               commonData.gradAtGaussPts[gg](2, 1);
        commonData.sTrain[5] = commonData.gradAtGaussPts[gg](0, 2) +
                               commonData.gradAtGaussPts[gg](2, 0);
        
      }
      /*MoFEMFunctionReturn(0);
      };*/

      commonData.Voigt_Unit.resize(6, false);
      commonData.Voigt_Unit.clear();
      
      for (int i = 0; i != 3; i++) {
        commonData.Voigt_Unit[i] = 1;
        commonData.Voigt_Unit[i + 3] = 0;
      }
      double Trace_sTrain =
          commonData.sTrain[0] + commonData.sTrain[1] + commonData.sTrain[2];
      double Trace_Plastic_Strain = 0.;
      for (int j = 0; j != 3; j++) {
        Trace_Plastic_Strain += commonData.Plastic_Strain[j + 6 * gg];
      }
      commonData.Dev_Total_Strain.clear();
      commonData.Dev_Total_Strain.resize(6, false);
      commonData.Dev_Total_Strain =
          commonData.sTrain - (1. / 3.) * Trace_sTrain * commonData.Voigt_Unit;
      commonData.Dev_Plastic_Strain.resize(nb_gauss_pts * 6);
      commonData.Dev_Plastic_Strain.clear();
      for (int i = 0; i != 6; i++) {
        commonData.Dev_Plastic_Strain[i + 6 * gg] =
            commonData.Plastic_Strain[i + 6 * gg] -
            (1. / 3.) * Trace_Plastic_Strain * commonData.Voigt_Unit[i];
      }
      VectorDouble6 elastic_strain;
      elastic_strain.resize(6);
      // CHKERR calculate_total_strain();

      commonData.Plastic_Strain_Temp[gg].clear();
      commonData.Plastic_Strain_Temp[gg].resize(6, false);
      for (int i = 0; i != 6; i++) {
        commonData.Plastic_Strain_Temp[gg][i] =
            commonData.Plastic_Strain[i + 6 * gg];
        // printf("Strain at %d is:%e\n",i ,
        // commonData.Plastic_Strain_Temp[gg][i] );
        elastic_strain[i] =
            commonData.sTrain[i] - commonData.Plastic_Strain_Temp[gg][i];
      }
      // VectorDouble6 elastic_strain =
      //   commonData.sTrain - commonData.Plastic_Strain;
     

      VectorDouble6 trial_stress = prod(commonData.D, elastic_strain);

      

      double sum_entries = 0;
      double g_Value = 0;
      double Dg_Value = 0;
     commonData.cOunter = 0;

      // double Trace_sTrain =
      // commonData.sTrain[0]+commonData.sTrain[1]+commonData.sTrain[2];  double
      // Trace_trial_stress = trial_stress[0]+trial_stress[1]+trial_stress[2];
      commonData.Dev_trial_stress.clear();
      commonData.Dev_trial_stress.resize(6, false);
      for (int j = 0; j != 6; j++) {
        commonData.Dev_trial_stress[j] =
            2 * commonData.lAme *
            (commonData.Dev_Total_Strain[j] -
             commonData.Dev_Plastic_Strain[j + 6 * gg]);
      }
  
      commonData.Flow_Vector.clear();
      commonData.Flow_Vector.resize(6, false);

      for (int j = 0; j < 6; j++) {
        sum_entries +=
            commonData.Dev_trial_stress[j] * commonData.Dev_trial_stress[j];
      }
      
      commonData.yIeld_Condition =
          sqrt(sum_entries) - sqrt(2. / 3.) * commonData.yIeld_Stress;

      commonData.pLastic_Multiplier = 0.;
     
      if (commonData.yIeld_Condition > 0) {
        for (int j = 0; j != 6; j++) {
          commonData.Flow_Vector[j] =
              commonData.Dev_trial_stress[j] / sqrt(sum_entries);
        }
        for (int i = 0; i < 2000; ++i) {
          g_Value = commonData.yIeld_Condition -
                    2 * commonData.lAme * commonData.pLastic_Multiplier;
          Dg_Value = -2 * commonData.lAme;
          commonData.pLastic_Multiplier =
              commonData.pLastic_Multiplier - g_Value / Dg_Value;

          if (fabs(g_Value) < commonData.tOlerance /*&& g_Value > 0.*/)
            break;
          commonData.cOunter++;
        } // while (fabs(g_Value) > commonData.tOlerance  && cOunter != 2000);
        
        for (int ii = 0; ii != 6; ii++) {
          commonData.Plastic_Strain_Temp[gg][ii] =
              commonData.Plastic_Strain_Temp[gg][ii] +
              commonData.pLastic_Multiplier * commonData.Flow_Vector[ii];
          commonData.Dev_Plastic_Strain[ii + 6 * gg] =
              commonData.Dev_Plastic_Strain[ii + 6 * gg] +
              commonData.pLastic_Multiplier * commonData.Flow_Vector[ii];
          commonData.Plastic_Strain[ii + 6 * gg] =
              commonData.Plastic_Strain[ii + 6 * gg] +
              commonData.pLastic_Multiplier * commonData.Flow_Vector[ii];
        }
        commonData.tHeta =
            1 - ((2 * commonData.lAme * commonData.pLastic_Multiplier) /
                 sqrt(sum_entries));
        commonData.tHeta_bar = commonData.tHeta;
        commonData.sTress[gg].clear();
        commonData.sTress[gg].resize(6, false);
        // noalias(commonData.sTress[gg]) = prod(commonData.D,
        // commonData.sTrain);
        commonData.sTress[gg] =
            commonData.kAppa * Trace_sTrain * commonData.Voigt_Unit +
            commonData.Dev_trial_stress -
            2 * commonData.lAme * commonData.pLastic_Multiplier *
                commonData.Flow_Vector;
        for (int i = 0; i != 6; i++) {
          for (int j = 0; j != 6; j++) {
            commonData.Flow_Tensor.insert_element(
                i, j,
                commonData.Dev_trial_stress[i] *
                    commonData.Dev_trial_stress[j]);
          }
        }
        commonData.Consistent_Matrix =
            commonData.kAppa * commonData.Voigt_Unit_Matrix +
            2 * commonData.lAme * commonData.tHeta *
                (commonData.Unit_Matrix -
                 (1. / 3.) * commonData.Voigt_Unit_Matrix) -
            2 * commonData.lAme * commonData.tHeta_bar * (1. / sum_entries) *
                commonData.Flow_Tensor;
        // commonData.D = commonData.Consistent_Matrix;
      }

     
      else {
        commonData.sTress[gg].clear();
        commonData.sTress[gg].resize(6, false);
        commonData.Consistent_Matrix = commonData.D;
        noalias(commonData.sTress[gg]) = prod(commonData.D, elastic_strain);
      }
    }

    MoFEMFunctionReturn(0);
  }
};

struct OpUpdate
    : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  MoFEM::Interface &mField;
  CommonData &commonData;

  OpUpdate(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data)
      : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
            field_name, UserDataOperator::OPROW),
        mField(m_field), commonData(common_data) {}

  PetscErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;

    if (type != MBVERTEX)
      PetscFunctionReturn(0);

    int nb_dofs = data.getFieldData().size();
    if (nb_dofs == 0)
      PetscFunctionReturn(0);
    int nb_gauss_pts    = data.getN().size1();
    EntityHandle fe_ent = getFEEntityHandle();
    CHKERR commonData.setPlasticStrain(fe_ent, nb_gauss_pts);

    MoFEMFunctionReturn(0);
  }
};

/*struct OpPostProcStress: public
MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

  CommonData &commonData;
  moab::Interface &postProcMesh;
  std::vector<EntityHandle> &mapGaussPts;

  OpPostProcStress(
      moab::Interface &post_proc_mesh,
    std::vector<EntityHandle> &map_gauss_pts,
    CommonData &common_data
  );

   PetscErrorCode doWork(
    int side,EntityType type,DataForcesAndSourcesCore::EntData &data
  ){
  //

  PetscFunctionBegin;

  if(type!=MBVERTEX) PetscFunctionReturn(9);
  double def_VAL[9];
  bzero(def_VAL,9*sizeof(double));

  Tag th_plastic_strain;
  rval = postProcMesh.tag_get_handle(
    "PLASTIC_STRAIN",9,MB_TYPE_DOUBLE,th_plastic_strain,MB_TAG_CREAT|MB_TAG_SPARSE,def_VAL
  ); CHKERRQ_MOAB(rval);

  }

};*/


#endif //__ELASTICITYNEW_HPP__
