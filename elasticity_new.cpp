/** \file elasticity.cpp
 * \ingroup nonlinear_elastic_elem
 * \example elasticity.cpp

 The example shows how to solve the linear elastic problem. An example can read
 file with temperature field, then thermal stresses are included.

 What example can do:
 - take into account temperature field, i.e. calculate thermal stresses and
deformation
 - stationary and time depend field is considered
 - take into account gravitational body forces
 - take in account fluid pressure
 - can work with higher order geometry definition
 - works on distributed meshes
 - multi-grid solver where each grid level is approximation order level
 - each mesh block can have different material parameters and approximation
order

See example how code can be used \cite jordi:2017,
 \image html SquelaDamExampleByJordi.png "Example what you can do with this
code. Analysis of the arch dam of Susqueda, located in Catalonia (Spain)"
width=800px

 This is an example of application code; it does not show how elements are
implemented. Example presents how to:
 - read mesh
 - set-up problem
 - run finite elements on the problem
 - assemble matrices and vectors
 - solve the linear system of equations
 - save results


 If you like to see how to implement finite elements, material, are other parts
of the code, look here;
 - Hooke material, see \ref Hooke
 - Thermal-stress assembly, see \ref  ThermalElement
 - Body forces element, see \ref BodyFroceConstantField
 - Fluid pressure element, see \ref FluidPressure
 - The general implementation of an element for arbitrary Lagrangian-Eulerian
 aformulation for a nonlinear elastic problem is here \ref
 NonlinearElasticElement. Here we limit ourselves to Hooke equation and fix
 mesh, so the problem becomes linear. Not that elastic element is implemented
 with automatic differentiation.

*/

/* MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <boost/program_options.hpp>
// #include "../poisson/src/AuxPoissonFunctions.hpp"
// #include "../poisson/src/PoissonOperators.hpp"
// #include <ElasticityNewOperator.hpp>
#include <Hooke.hpp>
#include <ElasticityNew.hpp>

using namespace boost::numeric;
using namespace MoFEM;
using namespace std;
namespace po = boost::program_options;

static char help[] = "-my_block_config set block data\n"
                     "\n";

int main(int argc, char *argv[]) {

  // Initialize PETSCc
  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);
  PetscInt nb_sub_steps = 10;
  // Create mesh database
  moab::Core mb_instance;              // create database
  moab::Interface &moab = mb_instance; // create interface to database

  try {
    // Create MoFEM database and link it to MoAB
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    CHKERR DMRegister_MoFEM("DMMOFEM");

    // Get command line options
    int order = 3;                    // default approximation order
    PetscBool flg_test = PETSC_FALSE; // true check if error is numerical error
    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "SimpleElasticProblem",
                             "none");
    // Set approximation order
    CHKERR PetscOptionsInt("-order", "approximation order", "", order, &order,
                           PETSC_NULL);
    // Set testing (used by CTest)
    CHKERR PetscOptionsBool("-test", "if true is ctest", "", flg_test,
                            &flg_test, PETSC_NULL);
    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    Simple *simple_interface;
    CHKERR m_field.query_interface(simple_interface);

    CHKERR simple_interface->getOptions();
    CHKERR simple_interface->loadFile();
    CHKERR simple_interface->addDomainField("U", H1, AINSWORTH_LEGENDRE_BASE,
                                            3);
    CHKERR simple_interface->setFieldOrder("U", order);

    // Range fix_faces, pressure_faces, fix_nodes, fix_second_node;

    // for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(m_field, BLOCKSET, bit))
    // {
    //   EntityHandle meshset = bit->getMeshset();
    //   int id = bit->getMeshsetId();

    //   if (id == 1) { // brick-faces

    //     rval = m_field.get_moab().get_entities_by_dimension(meshset, 2,
    //                                                         fix_faces, true);
    //     CHKERRQ_MOAB(rval);

    //     Range adj_ents;
    //     rval = m_field.get_moab().get_adjacencies(fix_faces, 0, false,
    //     adj_ents,
    //                                               moab::Interface::UNION);
    //     CHKERRQ_MOAB(rval);
    //     rval = m_field.get_moab().get_adjacencies(fix_faces, 1, false,
    //     adj_ents,
    //                                               moab::Interface::UNION);
    //     CHKERRQ_MOAB(rval);
    //     fix_faces.merge(adj_ents);
    //   } else if (id == 2) { // node(s)

    //     rval = m_field.get_moab().get_entities_by_dimension(meshset, 0,
    //                                                         fix_nodes, true);
    //     CHKERRQ_MOAB(rval);
    //   } else if (id == 3) { // brick pressure faces
    //     rval = m_field.get_moab().get_entities_by_dimension(
    //         meshset, 2, pressure_faces, true);
    //     CHKERRQ_MOAB(rval);
    //   } else if (id == 4) { // restrained second node in y direction
    //     rval = m_field.get_moab().get_entities_by_dimension(
    //         meshset, 0, fix_second_node, true);
    //     CHKERRQ_MOAB(rval);
    //   } else {
    //     SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY, "Unknown
    //     blockset");
    //   }
    // }

    CHKERR simple_interface->addDomainField("U", H1, AINSWORTH_LEGENDRE_BASE,
                                            3);

    CHKERR simple_interface->setFieldOrder("U", order);

    // Add pressure element
    // ierr = m_field.add_finite_element("PRESSURE");
    // CHKERRQ(ierr);
    // ierr = m_field.modify_finite_element_add_field_row("PRESSURE", "U");
    // CHKERRQ(ierr);
    // ierr = m_field.modify_finite_element_add_field_col("PRESSURE", "U");
    // CHKERRQ(ierr);
    // ierr = m_field.modify_finite_element_add_field_data("PRESSURE", "U");
    // CHKERRQ(ierr);

    CHKERR simple_interface->defineFiniteElements();
    CHKERR simple_interface->defineProblem();

    DM dm;

    CHKERR simple_interface->getDM(&dm);
    CHKERR MetaNeummanForces::addNeumannBCElements(m_field, "U");
    CHKERR MetaNodalForces::addElement(m_field, "U");
    CHKERR MetaEdgeForces::addElement(m_field, "U");
    CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
    // ierr = DMMoFEMAddElement(dm, "PRESSURE");
    // CHKERRQ(ierr);
    CHKERR DMMoFEMSetIsPartitioned(dm, PETSC_TRUE);
    CHKERR simple_interface->buildFields();
    CHKERR simple_interface->buildFiniteElements();
    CHKERR m_field.build_finite_elements("PRESSURE_FE");

    // ierr = m_field.add_ents_to_finite_element_by_dim(
    // 0, simple_interface->getDim(), simple_interface->getDomainFEName(),
    // true);
    // CHKERRQ(ierr);
    // ierr =
    // m_field.build_finite_elements(simple_interface->getDomainFEName());
    // CHKERRQ(ierr);
    // ierr = m_field.add_ents_to_finite_element_by_dim(pressure_faces, 2,
    //                                                  "PRESSURE");
    // CHKERRQ(ierr);
    // ierr = m_field.build_finite_elements("PRESSURE", &pressure_faces);
    // CHKERRQ(ierr);

    CHKERR simple_interface->buildProblem();

    CommonData commonData(m_field);
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> elastic_fe(
        new VolumeElementForcesAndSourcesCore(m_field));

    boost::shared_ptr<VolumeElementForcesAndSourcesCore> feLhs(
        new VolumeElementForcesAndSourcesCore(m_field));
    feLhs->getOpPtrVector().push_back(new OpGetDataAtGaussPts(
        "U", commonData.dataAtGaussPts, commonData.gradAtGaussPts));
    feLhs->getOpPtrVector().push_back(
        new OpCalculateStress(m_field, "U", commonData));

    feLhs->getOpPtrVector().push_back(new OpAssembleK(commonData));

    // push operators to elastic_fe
    // boost::shared_ptr<FaceElementForcesAndSourcesCore> pressure_fe(
    //     new FaceElementForcesAndSourcesCore(m_field));
    // pressure_fe->getOpPtrVector().push_back(new OpPressure());

    // boost::shared_ptr<FEMethod> fix_dofs_fe(
    //     new ApplyDirichletBc(fix_faces, fix_nodes, fix_second_node));

    boost::shared_ptr<FEMethod> nullFE;
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> feRhs(
        new VolumeElementForcesAndSourcesCore(m_field));

    feRhs->getOpPtrVector().push_back(new OpGetDataAtGaussPts(
        "U", commonData.dataAtGaussPts, commonData.gradAtGaussPts));
    feRhs->getOpPtrVector().push_back(
        new OpCalculateStress(m_field, "U", commonData));
    feRhs->getOpPtrVector().push_back(new OpAssembleRhs(commonData));
    // // Set operators for KSP solver
    // ierr = DMMoFEMKSPSetComputeOperators(
    //     dm, simple_interface->getDomainFEName(), elastic_fe, nullFE, nullFE);
    // ierr = DMMoFEMKSPSetComputeRHS(dm, "PRESSURE", pressure_fe, nullFE,
    // nullFE);
    // CHKERRQ(ierr);
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> feUpdate(
        new VolumeElementForcesAndSourcesCore(m_field));

    feUpdate->getOpPtrVector().push_back(new OpGetDataAtGaussPts(
        "U", commonData.dataAtGaussPts, commonData.gradAtGaussPts));
    feUpdate->getOpPtrVector().push_back(
        new OpCalculateStress(m_field, "U", commonData));
    feUpdate->getOpPtrVector().push_back(new OpUpdate(m_field, "U", commonData));

    Mat Aij;
    Vec d, F_ext;
    Vec F_init;

    {
      CHKERR DMCreateGlobalVector_MoFEM(dm, &d);
      CHKERR VecDuplicate(d, &F_ext);
      CHKERR VecDuplicate(d, &F_init);
      CHKERR DMCreateMatrix_MoFEM(dm, &Aij);
      CHKERR VecZeroEntries(d);
      CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToLocalVector(
          dm, d, INSERT_VALUES, SCATTER_REVERSE); // TODO: ask, global/local?
      CHKERR MatZeroEntries(Aij);
    }

    // ierr = DMCreateMatrix(dm, &Aij);
    // CHKERRQ(ierr);
    // ierr = DMCreateGlobalVector(dm, &d);
    // CHKERRQ(ierr);
    // ierr = VecZeroEntries(d);
    // CHKERRQ(ierr);
    // ierr = VecDuplicate(d, &F_ext);
    // ierr = VecDuplicate(d, &F_init);
    // CHKERRQ(ierr);

    // ierr = VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
    // CHKERRQ(ierr);
    // ierr = VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
    // CHKERRQ(ierr);
    // ierr = DMoFEMMeshToLocalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
    // CHKERRQ(ierr);
    // ierr = MatZeroEntries(Aij);
    // CHKERRQ(ierr);

    // elastic_fe->snes_B = Aij;
    // elastic_fe->snes_f = F_ext;
    boost::shared_ptr<DirichletDisplacementBc> dirichlet_bc_ptr(
        new DirichletDisplacementBc(m_field, "U", Aij, d, F_ext));
    dirichlet_bc_ptr->methodsOp.push_back(new LoadScale());

    // dirichlet_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE; //for KSP solver
    // dirichlet_bc_ptr->ts_ctx = FEMethod::CTX_TSNONE;
    // ierr = DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
    // CHKERRQ(ierr);
    // ierr = DMoFEMLoopFiniteElements(dm, simple_interface->getDomainFEName(),
    //                                 elastic_fe);
    // CHKERRQ(ierr);

    // Assemble pressure and traction forces. Run particular implemented for do
    // this, see
    // MetaNeummanForces how this is implemented.
    boost::ptr_map<std::string, NeummanForcesSurface> neumann_forces;
    CHKERR MetaNeummanForces::setMomentumFluxOperators(
        m_field, neumann_forces, NULL, "U"); // TODO: ask F
    {
      auto fit = neumann_forces.begin();
      for (; fit != neumann_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale());
      }
    }


    // CHKERRQ(ierr);
    // {
    //   boost::ptr_map<std::string, NeummanForcesSurface>::iterator mit =
    //       neumann_forces.begin();
    //   for (; mit != neumann_forces.end(); mit++) {
    //     ierr = DMoFEMLoopFiniteElements(dm, mit->first.c_str(),
    //                                     &mit->second->getLoopFe());
    //     CHKERRQ(ierr);
    //   }
    // }
    // // Assemble forces applied to nodes, see implementation in NodalForce
    boost::ptr_map<std::string, NodalForce> nodal_forces;
    CHKERR MetaNodalForces::setOperators(m_field, nodal_forces, NULL, "U");
    {
      auto fit = nodal_forces.begin();
      for (; fit != nodal_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale()); 
      }
    }
    // CHKERRQ(ierr);
    // {
    //   boost::ptr_map<std::string, NodalForce>::iterator fit =
    //       nodal_forces.begin();
    //   for (; fit != nodal_forces.end(); fit++) {
    //     ierr = DMoFEMLoopFiniteElements(dm, fit->first.c_str(),
    //                                     &fit->second->getLoopFe());
    //     CHKERRQ(ierr);
    //   }
    // }
    // // Assemble edge forces
    boost::ptr_map<std::string, EdgeForce> edge_forces;
    CHKERR MetaEdgeForces::setOperators(m_field, edge_forces, NULL, "U");
    {
      auto fit = edge_forces.begin();
      for (; fit != edge_forces.end(); fit++) {
        fit->second->methodsOp.push_back(new LoadScale());
      }
    }

    // CHKERRQ(ierr);
    // {
    //   boost::ptr_map<std::string, EdgeForce>::iterator fit =
    //       edge_forces.begin();
    //   for (; fit != edge_forces.end(); fit++) {
    //     ierr = DMoFEMLoopFiniteElements(dm, fit->first.c_str(),
    //                                     &fit->second->getLoopFe());
    //     CHKERRQ(ierr);
    //   }
    // }

    // ierr = DMoFEMLoopFiniteElements(dm, "PRESSURE", pressure_fe);
    // CHKERRQ(ierr);
    // This is done because only post processor is implemented in the
    // ApplyDirichletBc struct
    // ierr = DMoFEMPostProcessFiniteElements(dm, dirichlet_bc_ptr.get());
    // CHKERRQ(ierr);
    // ierr = DMoFEMPostProcessFiniteElements(dm, fix_dofs_fe.get());

    boost::shared_ptr<FEMethod> fe_null;

    // Rhs
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                  fe_null);

    {
      auto fit = neumann_forces.begin();
      for (; fit != neumann_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }
    {
      auto fit = nodal_forces.begin();
      for (; fit != nodal_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }
    {
      auto fit = edge_forces.begin();
      for (; fit != edge_forces.end(); fit++) {
        CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                      &fit->second->getLoopFe(), NULL, NULL);
      }
    }

    dirichlet_bc_ptr->snes_ctx = SnesMethod::CTX_SNESNONE;
    dirichlet_bc_ptr->snes_x = d;
    CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
    CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);

    CHKERR DMMoFEMSNESSetFunction(dm,
                                  simple_interface->getDomainFEName().c_str(),
                                  feRhs, fe_null, fe_null);
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                  dirichlet_bc_ptr);
    // Lhs
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                  fe_null);
    CHKERR DMMoFEMSNESSetJacobian(dm,
                                  simple_interface->getDomainFEName().c_str(),
                                  feLhs, fe_null, fe_null);
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                  dirichlet_bc_ptr);

    SNESConvergedReason snes_reason;
    SNES snes;
    SnesCtx *snes_ctx;
    {
      CHKERR SNESCreate(PETSC_COMM_WORLD, &snes);
      // ierr = SNESSetDM(snes,dm); CHKERRQ(ierr);
      CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
      CHKERR SNESSetFunction(snes, F_ext, SnesRhs, snes_ctx);
      CHKERR SNESSetJacobian(snes, Aij, Aij, SnesMat, snes_ctx);
      CHKERR SNESSetFromOptions(snes);
    }

    CHKERR VecAssemblyBegin(d);
    CHKERR VecAssemblyEnd(d);

    // Vec F_int;
    // ierr = VecDuplicate(x,&f0); CHKERRQ(ierr);
    // ierr = DMoFEMMeshToLocalVector(dm,f0,INSERT_VALUES,SCATTER_FORWARD);
    // CHKERRQ(ierr);

    PostProcVolumeOnRefinedMesh post_proc(m_field);
    CHKERR post_proc.generateReferenceElementMesh();
    CHKERR post_proc.addFieldValuesPostProc("U");
    CHKERR post_proc.addFieldValuesGradientPostProc("U");
   
    int nb_sub_steps = 20; // this should be option from command line
    double step_size = 1. / nb_sub_steps;
    double lambda0 = 0.05;
    for (int ss = 0; ss <= nb_sub_steps; ++ss) {
      LoadScale::lambda = (ss + 1) * lambda0;

      dirichlet_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE; 
      dirichlet_bc_ptr->snes_x = d;
      CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
      CHKERR VecAssemblyBegin(d);
      CHKERR VecAssemblyEnd(d);

      // CHKERR VecCopy(d, d0);
      // CHKERR VecAXPY(d, step_size, d0);
      // CHKERR DMoFEMMeshToLocalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
      // printf("Time is %d in pre", ss);
      // CHKERR VecView(d, PETSC_VIEWER_STDOUT_WORLD);
      CHKERR SNESSolve(snes, PETSC_NULL, d);
      CHKERR SNESGetConvergedReason(snes, &snes_reason);
      int its;
      CHKERR SNESGetIterationNumber(snes, &its);
      CHKERR PetscPrintf(PETSC_COMM_WORLD,
                         "number of Newton iterations = %D\n\n", its);

       CHKERR DMoFEMLoopFiniteElements(
           dm, simple_interface->getDomainFEName().c_str(), feUpdate);


      CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
      // printf("Time is %d in post", ss);
      // CHKERR VecView(d, PETSC_VIEWER_STDOUT_WORLD);
      // Save data on mesh

      CHKERR DMoFEMLoopFiniteElements(
          dm, simple_interface->getDomainFEName().c_str(), &post_proc);
      string out_file_name;
      std::ostringstream stm;
      stm << "out_" << ss << ".h5m";
      out_file_name = stm.str();
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "out file %s\n",
                         out_file_name.c_str());
      CHKERR post_proc.postProcMesh.write_file(out_file_name.c_str(), "MOAB",
                                               "PARALLEL=WRITE_PART");
    }
    /*ierr = DMoFEMMeshToGlobalVector(dm,d,INSERT_VALUES,SCATTER_REVERSE
                    ); CHKERRQ(ierr);
    ierr =
   DMoFEMLoopFiniteElements(dm,simple_interface->getDomainFEName().c_str(),feRhs.get()
                   ); CHKERRQ(ierr);
  }*/
    //CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
    //CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
    //CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
    //  ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
    //  std::string wait;
    //  std::cin >> wait;
    // cerr << " RHS vector \n";
    // VecView(F_ext, PETSC_VIEWER_STDOUT_WORLD);
    CHKERR SNESDestroy(&snes);

    // cerr << " RHS vector \n";
    // VecView(F_init, PETSC_VIEWER_STDOUT_WORLD);
    // MatView(Aij, PETSC_VIEWER_DRAW_WORLD);
    // save solution in vector x on mesh

    // Set up post-procesor. It is some generic implementation of finite
    // element.

    // Add operators to the elements, starting with some generic

    /* ierr = post_proc.generateReferenceElementMesh();
     CHKERRQ(ierr);
     ierr = post_proc.addFieldValuesPostProc("U");
     CHKERRQ(ierr);
     ierr = post_proc.addFieldValuesGradientPostProc("U");
     CHKERRQ(ierr);
     ierr = DMoFEMLoopFiniteElements(
         dm, simple_interface->getDomainFEName().c_str(), &post_proc);
     CHKERRQ(ierr);
     ierr = post_proc.writeFile("out.h5m");
     CHKERRQ(ierr);*/

    CHKERR MatDestroy(&Aij);
    CHKERR VecDestroy(&d);
    CHKERR VecDestroy(&F_ext);
    CHKERR VecDestroy(&F_init);
    CHKERR DMDestroy(&dm);

    // This is a good reference for the future
  }
  CATCH_ERRORS;

  // finish work cleaning memory, getting statistics, etc
  MoFEM::Core::Finalize();
  // cerr<<elastic_strain<<"\n";
  return 0;
}
